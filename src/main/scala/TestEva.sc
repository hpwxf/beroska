case class X(i: Integer) {
  def %+%(j: Integer) : Integer = { i+j+1 }
}

val x = X(2)

x %+% 2

