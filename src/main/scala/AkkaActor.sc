

println("Starting")

class SimpleActor extends Actor {
  override def receive = {
    case s: String => println("Receive String:" + s)
    case i: Int => println("Receice Int:" + i)
  }

  def foo = println("Foo")
}

val system: ActorSystem = ActorSystem("SimpleSystem", ConfigFactory.load())
val actor = system.actorOf(Props[SimpleActor], "SimpleActor")

actor ! "Hi there"
actor ! 42
