  println("Hello world")

  val range = 1 to 10

  println(range)

  val x = range foreach println

  def f(x: Int) : Int = 4 * x



  val product = range map {x : Int => x*5 }

  println(product.reverse)


  var a = "hello world".toList

  a.sliding(2,2)


  var voyelles : Set[Char] =Set('a','e','i','o','u','y')


  def addVoyelle(l: List[String], x: Char): List[String] = {
    if (l.isEmpty) {
      List(x.toString)
    } else {
      l.init ::: List(l.last + x)
    }
  }

  def addConsonne(l: List[String], x: Char): List[String] = {
    if (l.isEmpty) {
      List(x.toString)
    } else if (voyelles.contains(l.last.last)) {
      l ::: List(x.toString)
    } else {
      l.init ::: List(l.last + x)
    }
  }

  val m = "helloi world".toList.foldLeft(List[String]()) {
    (s, x) =>
      voyelles.find(_ == x) match {
        case Some(_) => addVoyelle(s,x)
        case None => addConsonne(s,x)
      }
  }

  println(m)


  val h = hyphen.Hyphenator.load("en")
  val m2 = h.hyphenate("hello world")
  println(m2)







    val numbers = range.toList

    def sum(list : List[Int], selector : Int => Boolean = x => true) : Int = list.filter(selector).foldLeft(0) { (c,e) => c + e }


    println("Result1 is " + sum(numbers, x => x > 5))
    println("Result2 is " + range.map { _ + 0 }.sum)

    println("Result3 is " +
      (0 /: numbers) { _ + _ })

    // ??? // Undefined : to do
    val z = (1 until 10).find( _ > 51)
    println("Result4 is " + z.getOrElse(Unit))


    // Lazy variable
    println("------------")
    lazy val lazy_a = { println("lazy a"); 3 }
    val eager_a = { println("eager a"); 3 }
    println("------------")
    println(lazy_a)
    println(eager_a)
    println("------------")
