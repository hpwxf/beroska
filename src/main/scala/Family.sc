
// -- people

sealed abstract class Individual(val s: String) // abstract class may have ctor arg

trait Immortality { // no ctor for trait
  override def toString = s"Immortal " + super.toString
}

trait Invisibility {
  override def toString = s"Invisible " + super.toString
  def invisibleFactor : Double // undefined behaviour; must be done by a derived class
}

class Human(name: String) extends Individual(name) {
  override def toString = s"Human($name)"
}

// non case class may be extended
class Idiot(name: String) extends Human(name) {
  private[this] var stupidThing = 0
  def doStupid : Unit = { stupidThing = stupidThing+1 }
  override def toString = s"Idiot($name) with stupidity score : $stupidThing"
}

class Alien(name: String) extends Individual(name) with Immortality {
  override def toString = s"Alien($name)"
  def invisibleFactor = 1.0
}

class Other(name: String, val kind: String) extends Individual(name) {
  override def toString = s"$kind($name)"
}

// -- relations

object RelationKindEnum extends Enumeration {
  type RelationKindEnum = Value
  val Descendant, Sibling, None, Undef = Value
}

sealed abstract class RelationKind(val kind: RelationKindEnum.RelationKindEnum, degree: Int)
object RelationKind {
  case object Brother extends RelationKind(RelationKindEnum.Sibling, 0)
  case object Child extends RelationKind(RelationKindEnum.Descendant, 1)
  case object Spouse extends RelationKind(RelationKindEnum.Sibling, 1)
  case object Undefined extends RelationKind(RelationKindEnum.Undef, 0)
  // case class ctor args are implicitly attributes
  case class Other(override val kind: RelationKindEnum.RelationKindEnum, degree: Int)
    extends RelationKind(kind, degree)
}

// -- custom operators

case class Relation(a: Individual, b: Individual, kind: RelationKind) {
  def @@(r : RelationKind) : Relation = Relation(a,b,r)
}

implicit final class RelationOperator(private val self: Individual) {
  def --> (y: Individual): Relation = Relation(self, y, RelationKind.Undefined)
}

// -- Family

// Ctor args are explicitly attributes in class
class Family(val name: String, val members: Set[Individual], val relations: List[Relation])
{
  override def toString = s"Family $name:\n" + members.toList.map { "\t" + _.toString + "\n" }.mkString

  def populate : Family = {
    val newGens : List[Family] = relations.flatMap {
      // Who's who : a & b ?
      case r @ Relation(a,b, RelationKind.Spouse) => {
        val x : Individual = produce(r.a,r.b)
        List(new Family(name, Set(x), (a-->x) @@ RelationKind.Child :: (b-->x) @@ RelationKind.Child :: Nil))
      }
      case _ => List()
    }
    newGens.foldLeft(this) { (b,a) => b + a }
  }

  def +(that : Family) : Family = {
    new Family(name, this.members ++ that.members, this.relations ::: that.relations)
  }

  private[this] def produce(a: Individual, b: Individual) : Individual = {
    if (math.random < 0.5)
      new Other(scala.util.Random.alphanumeric.take(5).mkString,"NewGen")
    else
      new Other(scala.util.Random.alphanumeric.take(5).mkString,"NewGen") with Immortality
  }
}


// -- let's play

// Question : for each previous class or trait, what can we do with them ?
// Question : for following code, predict the value of the variable and the displayed info

val a : Individual = new Human("Jacques")
val b : Individual = new Other("SupeR","Kryptonian") with Immortality
val c : Individual = new Alien("Roger")
val d : Individual = new Other("Yamka","Chèvre")
val e : Idiot = new Idiot("muddy")
val f : Individual = new Individual("Unique") with Invisibility {
  override def toString = "I'am Unique"
  def invisibleFactor : Double = 0.99
}

// Idiot is working
for(i <- 1 to 4) e.doStupid

// ctor with named args
val family : Family = new Family(
  name = "CL",
  members = Set(a,b,c,d,e,f),
  relations = (a --> b) @@ RelationKind.Child :: (a--> c) @@ RelationKind.Spouse :: Nil
)

println(family)

family.relations foreach { println(_) }

family.populate.populate

val humans : Set[Human] = family.members.collect { case a : Human => a }

def findThem[A <: Human](s: Set[A]) : List[Idiot] =
  s.toList.collect { case a : Idiot => a }

// findThem(family.members) // compilation fails
val they = findThem(humans)

val evenBetterThey = for (x <- they) yield { x.doStupid; x }