import java.util.Calendar

implicit class IntUtils(val number : Int) {
  def days = this

  def ago = {
    val today= Calendar.getInstance()
    today.add(Calendar.DAY_OF_MONTH, -number)
    today.getTime()
  }
}

// old style
// implicit def int2IntUtil(number : Int) = new IntUtils(number)

println(2.days.ago)

