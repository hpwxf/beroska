import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}

val promise = Promise[Int]
val future = promise.future
future.value
promise.success(42)
future.value

val success : Future[Int] = Future { 42 / 1}
val failure : Future[Int] = Future { 42 / 0}


val x = success andThen {
  case Success(res) => { println(res); 2 }
  case Failure(ex) => { println(ex); 3.4 }
}


//val y : Future[Int] =
  success onComplete {
  case Success(res) => { println(res); 2 }
  case Failure(ex) => { println(ex); 3.4 }
}