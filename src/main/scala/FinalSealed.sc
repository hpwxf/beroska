type Id = Int
type Email = String

sealed trait Visitor {
  def id: Int
}

object Visitor {
  def info(visitor: Visitor): Unit = {
    visitor match {
      case User(id, email) => println(s"User with id ${id} and email ${email}")
      case Anonymous(_) => println("Anonymous")
      // case _ => println("Undefined visitor")
    }
  }
}

final case class Anonymous(id: Id) extends Visitor

final case class User(id: Id, email: Email) extends Visitor


var x: Visitor = User(10, "me@gmail.com")
Visitor.info(x)

x = Anonymous(10)
Visitor.info(x)
