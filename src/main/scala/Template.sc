def count1[T](elements: Set[T]): Int = elements.size
count1(Set[Int](1,2,3))

def count2(elements: Set[T] forSome {type T}): Int = elements.size
count2(Set[Int](1,2,3))

def count3(elements: Set[_]): Int = elements.size
count3(Set[Int](1,2,3))

def sum[T<%Double](elements: Set[T]): Double =
  (0.0 /: elements)((d1,d2) => d1 + d2)
sum(Set[Int](1,2,3))
sum(Set[Float](1.0f,2.5f))