package utils {
  package silhouette {
    sealed abstract class ServiceAuthorization(val repr: String) {
      override def toString: String = repr
    }
    object ServiceAuthorization {
      case object Demo extends ServiceAuthorization("Demo")
      case object Public extends ServiceAuthorization("Public")
      case object Member extends ServiceAuthorization("Member")
      case object Premium extends ServiceAuthorization("Premium")
      case object Community extends ServiceAuthorization("Community")
      case object Staff extends ServiceAuthorization("Staff")
      case object Business extends ServiceAuthorization("Business")
      case object Developer extends ServiceAuthorization("Developer")
      case object Admin extends ServiceAuthorization("Admin")
      case object Master extends ServiceAuthorization("Master")
      case object Undefined extends ServiceAuthorization("Undefined") // only to manage silent decoding error

      def fromString(value: String): Option[ServiceAuthorization] = {
        values.find(_.toString == value)
      }
      val values = Seq(Demo, Public, Member, Premium, Community, Staff, Business, Developer, Admin, Master)
      val commonServices = Seq(Demo, Public, Member, Premium, Community, Staff, Business, Developer, Admin)
    }  }
}

package object models {
  object UserStatus extends Enumeration {
    type UserStatus = Value
    val Subscribing, Validating, Accepted, Rejected, Disabled = Value
  }

  object VisibilityLevel extends Enumeration {
    type VisibilityLevel = Value
    val Banished, Public, Known, Guest, Initiated, Close, Private = Value
  }
}

case class DbUser(id: Int,
                  email: Option[String] = None,
                  firstName: Option[String] = None,
                  lastName: Option[String] = None,
                  fullName: Option[String] = None,
                  avatarUrl: Option[String] = None,
                  activated: Boolean = false,
                  services: List[utils.silhouette.ServiceAuthorization],
                  creationDate: java.time.LocalDateTime,
                  updateDate: java.time.LocalDateTime,
                  status: models.UserStatus.UserStatus)

case class DbUserProfile(
    userId: Int,
    data: Option[play.api.libs.json.JsValue] = None,
    fragmentDisplay: Option[play.api.libs.json.JsValue] = None,
    grabberDisplay: Option[play.api.libs.json.JsValue] = None,
    matchingCriteria: Option[play.api.libs.json.JsValue] = None)

case class DbUserGrabber(
    id: Int,
    userId: Int,
    data: play.api.libs.json.JsValue,
    tags: List[String],
    suppressionDate: Option[java.time.LocalDateTime] = None)

case class DbUserInteraction(
    userId: Int,
    otherUserId: Int,
    level: Option[models.VisibilityLevel.VisibilityLevel],
    lastVisitDate: java.time.LocalDateTime,
    history: Option[play.api.libs.json.JsValue] = None)

object AbsttractionMembers extends App {}
